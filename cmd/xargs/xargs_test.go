package xargs

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"bufio"
	"bytes"
	"strings"
)

func TestXargs(t *testing.T) {
	t.Run("rate=2 inflight=1", func(t *testing.T) {
		rate := uint64(2)
		inflight := uint64(1)

		t.Run("One line", func(t *testing.T) {
			inStrings := []string{"test"}
			in := strings.NewReader(strings.Join(inStrings, "\n"))
			var outBuffer bytes.Buffer
			out := bufio.NewWriter(&outBuffer)

			Xargs(rate, inflight, []string{"echo", "{}"}, in, out)
			out.Flush()

			result := extractResult(outBuffer)
			assert.Equal(t, inStrings, result)
		})
		t.Run("Two lines", func(t *testing.T) {
			inStrings := []string{"test", "cat"}
			in := strings.NewReader(strings.Join(inStrings, "\n"))
			var outBuffer bytes.Buffer
			out := bufio.NewWriter(&outBuffer)
			cmdParts := []string{"echo", "{}"}

			Xargs(rate, inflight, cmdParts, in, out)
			out.Flush()

			result := extractResult(outBuffer)
			assert.Equal(t, inStrings, result)
		})
	})

	t.Run("rate=2 inflight=2", func(t *testing.T) {
		rate := uint64(2)
		inflight := uint64(2)

		// Sleep a bit and execute then
		cmdParts := []string{"perl", "-E", `$|=1;system("sleep",$ARGV[1]);say($ARGV[0]);`, "{}"}

		t.Run("Two lines", func(t *testing.T) {
			inStrings := []string{"test 0", "cat 0.1"}
			in := strings.NewReader(strings.Join(inStrings, "\n"))
			var outBuffer bytes.Buffer
			out := bufio.NewWriter(&outBuffer)

			Xargs(rate, inflight, cmdParts, in, out)
			out.Flush()

			expected := []string{"test", "cat"}
			result := extractResult(outBuffer)
			assert.Equal(t, expected, result)
		})
		t.Run("Two lines in other order", func(t *testing.T) {
			inStrings := []string{"test 0.1", "cat 0"}
			in := strings.NewReader(strings.Join(inStrings, "\n"))
			var outBuffer bytes.Buffer
			out := bufio.NewWriter(&outBuffer)

			Xargs(rate, inflight, cmdParts, in, out)
			out.Flush()

			expected := []string{"cat", "test"}
			result := extractResult(outBuffer)
			assert.Equal(t, expected, result)
		})
	})
}

func extractResult(buf bytes.Buffer) []string {
	str := strings.TrimSpace(string(buf.Bytes()))
	return strings.Split(str, "\n")
}
