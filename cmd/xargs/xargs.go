package xargs

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"
)

func XargsStd(rate, inflight uint64, cmdParts []string) {
	Xargs(rate, inflight, cmdParts, os.Stdin, os.Stdout)
}

func Xargs(rate, inflight uint64, cmdParts []string, in io.Reader, out io.Writer) {
	if len(cmdParts) < 2 {
		log.Fatal("Not enough arguments to Xargs")
	}

	reader := bufio.NewReader(in)

	limitRateC := make(chan struct{})
	defer close(limitRateC)

	limitInFlightC := make(chan struct{}, inflight)
	defer close(limitInFlightC)

	quitC := make(chan struct{})
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		limitRateManage(rate, limitRateC, quitC)
		wg.Done()
	}()

	applyLineByLine(reader, func(line string, isEOF bool) {
		line = strings.TrimSpace(line)

		if len(line) > 0 {
			limitRateWaitIfFedUp(limitRateC)
			limitInFlightWaitIfFedUp(limitInFlightC)

			wg.Add(1)
			go func() {
				runCmd(cmdParts, line, out)

				limitInFlightGetHungry(limitInFlightC)
				wg.Done()
			}()
		}
	})

	close(quitC)
	wg.Wait()
}

func limitRateWaitIfFedUp(limitRateC chan struct{}) {
	<-limitRateC
}

func limitRateManage(rate uint64, limitRateC, quitC chan struct{}) {
	oneSecondPassed := time.NewTicker(time.Second)
	for {
		for i := 0; i < int(rate); i++ {
			select {
			case limitRateC <- struct{}{}:
			case <-quitC:
				return
			}
		}

		select {
		case <-oneSecondPassed.C:
		case <-quitC:
			oneSecondPassed.Stop()
			return
		}
	}
}

func limitInFlightWaitIfFedUp(limitInFlightC chan struct{}) {
	limitInFlightC <- struct{}{}
}

func limitInFlightGetHungry(limitInFlightC chan struct{}) {
	<-limitInFlightC
}

func getCmdArgs(cmdParts []string, line string) []string {
	cmdArgs := make([]string, 0, len(cmdParts))
	for _, cmdPart := range cmdParts {
		str := strings.Replace(cmdPart, "{}", line, -1)
		// TODO ignore spaces inside quotes
		for _, arg := range strings.Split(str, " ") {
			arg = strings.TrimSpace(arg)
			cmdArgs = append(cmdArgs, arg)
		}
	}

	return cmdArgs
}

func runCmd(cmdParts []string, line string, out io.Writer) {
	cmdArgs := getCmdArgs(cmdParts, line)

	cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}

	reader := bufio.NewReader(stdout)
	applyLineByLine(reader, func(line string, isEOF bool) {
		if len(line) > 0 || !isEOF {
			fprintlnAtomic(out, line)
		}
	})

	err = cmd.Wait()
	if err != nil {
		log.Fatal(err)
	}
}

type handleLineF func(line string, isEOF bool)

func applyLineByLine(reader *bufio.Reader, handleLine handleLineF) {
	for {
		line, err := reader.ReadString('\n')
		if err != nil && err != io.EOF {
			break
		}

		isEOF := err == io.EOF
		handleLine(line, isEOF)

		if err == io.EOF {
			break
		}
	}
}

var mu sync.Mutex

func fprintlnAtomic(out io.Writer, line string) {
	mu.Lock()
	defer mu.Unlock()

	line = strings.TrimSuffix(line, "\n")

	fmt.Fprintln(out, line)
}
