package main

import (
	"flag"

	"ratelimit/cmd/xargs"
)

var rate uint64
var inflight uint64

func init() {
	flag.Uint64Var(&rate, "rate", 1, "Limits maximum number of commands handled per second")
	flag.Uint64Var(&inflight, "inflight", 1, "Limits maximum number of commands handled concurently")
}

func main() {
	flag.Parse()

	xargs.XargsStd(rate, inflight, flag.Args())
}
