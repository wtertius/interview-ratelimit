FROM nginx

RUN apt-get update && \
    apt-get install -y wget git make gcc watch

ARG goversion=1.11.2
RUN mkdir /usr/share/libs && \
    wget -cnv "https://storage.googleapis.com/golang/go${goversion}.linux-amd64.tar.gz" -O /usr/share/libs/go${goversion}.linux-amd64.tar.gz && \
    tar -xvzf /usr/share/libs/go${goversion}.linux-amd64.tar.gz -C /usr/local

ARG GOPATH=/root/go
ARG GOBIN=${GOPATH}/bin
ENV PATH "${GOBIN}:/usr/local/go/bin:${PATH}"

ARG GLIDE_PATH=${GOPATH}/src/github.com/Masterminds/glide
ARG GLIDE_VERSION=v0.12.3
ARG GLIDE_BIN_NAME=glide-${GLIDE_VERSION}
ARG GLIDE_BIN=${GOBIN}/${GLIDE_BIN_NAME}

RUN mkdir -p ${GOPATH}/src ${GOPATH}/pkg ${GOBIN}

RUN mkdir -p ${GLIDE_PATH} && cd ${GLIDE_PATH} ;\
    git clone https://github.com/Masterminds/glide.git . ;\
    cd ${GLIDE_PATH} && git fetch --tags && git checkout ${GLIDE_VERSION} ;\
    git reset --hard && git clean -fd ;\
    make clean && make build && mv ${GLIDE_PATH}/glide ${GLIDE_BIN} ;\
    cd ${GOBIN} && ln -s ${GLIDE_BIN_NAME} glide

RUN go get github.com/githubnemo/CompileDaemon
RUN go get github.com/derekparker/delve/cmd/dlv
RUN go get github.com/k0kubun/pp

ARG DIR=/root/go/src/ratelimit
WORKDIR ${DIR}

ADD ./glide.lock ./glide.yaml ${DIR}/
RUN glide install

ARG BIN=${DIR}/bin/web
CMD go build -o ${BIN} -i ${DIR}/cmd/main.go \
    && ${BIN}
